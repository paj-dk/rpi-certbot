# Certbot 1.0

FROM armhf/alpine:latest
MAINTAINER pajdk "https://hub.docker.com/r/pajdk/rpi-certbot/"

EXPOSE 443

RUN apk --update add certbot bash

ENTRYPOINT ["certbot"]
